package com.github.spanslator.security;

import com.github.spanslator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RefererRedirectionAuthenticationSuccessHandler refererRedirectionAuthenticationSuccessHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/api/**").authenticated()
				.antMatchers("/login").permitAll()
				.antMatchers("/register").permitAll()
				.anyRequest().permitAll()
				.and()
				.httpBasic() //This line will enable Basic Authentication as well. It can be used to access REST API from Postman using Basic Authentication
				.and()
				.formLogin().loginPage("/login").usernameParameter("username")
				.passwordParameter("password").failureUrl("/login?error")
				.successHandler(refererRedirectionAuthenticationSuccessHandler)
				.defaultSuccessUrl("/")
				.and()
				.logout().logoutRequestMatcher(new AntPathRequestMatcher("logout")).logoutSuccessUrl("/login")
				.and()
				.csrf().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService)
				.passwordEncoder(getPasswordEncoder());
	}

	private PasswordEncoder getPasswordEncoder() {
		return new PasswordEncoder() {

			@Override
			public String encode(final CharSequence rawPassword) {
				return passwordEncoder.encode(rawPassword);
			}

			@Override
			public boolean matches(final CharSequence rawPassword, final String encodedPassword) {
				return passwordEncoder.matches(rawPassword, encodedPassword);
			}
		};
	}

}
