package com.github.spanslator.security;

import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class RefererRedirectionAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	public RefererRedirectionAuthenticationSuccessHandler() {
		setUseReferer(true);
	}
}
