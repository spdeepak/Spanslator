package com.github.spanslator.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;
import org.springframework.http.HttpStatus;

/**
 * @author Deepak Sunanda Prabhakar
 */
@JsonView({ Views.JustProjects.class, Views.JustGlossaries.class, Views.JustTranslations.class,
		Views.JustLanguages.class, Views.JustLanguagesExceptEnabledField.class })
public class ApiResponse {

	private HttpStatus status;

	private String message;

	public ApiResponse() {
	}

	public ApiResponse(HttpStatus status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
}
