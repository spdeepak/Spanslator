package com.github.spanslator.api.json.views;

/**
 * @author Deepak Sunanda Prabhakar
 */
public class Views {

	public static class JustProjects {

	}

	public static class JustGlossaries {

	}

	public static class JustTranslations {

	}

	public static class JustLanguages {

	}

	public static class JustLanguagesExceptEnabledField {

	}
}
