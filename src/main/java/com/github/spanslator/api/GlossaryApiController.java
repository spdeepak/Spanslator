package com.github.spanslator.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;
import com.github.spanslator.entity.Glossary;
import com.github.spanslator.entity.Language;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import com.github.spanslator.service.GlossaryService;
import com.github.spanslator.service.LanguageService;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.TranslationService;
import com.github.spanslator.util.FormString;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Deepak Sunanda Prabhakar
 */
@RestController
@RequestMapping("/api")
@SuppressWarnings("unchecked")
@Api(tags = { "Glossary" })
public class GlossaryApiController {

	private static final Logger logger = LoggerFactory.getLogger(GlossaryApiController.class);

	@Autowired
	private GlossaryService glossaryService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TranslationService translationService;

	@Autowired
	private LanguageService languageService;

	@GetMapping("/project/{projectId}/language/{locale}/glossary")
	@JsonView(Views.JustGlossaries.class)
	public ResponseEntity getAllGlossariesForProjectAndLocale(@PathVariable("projectId") Long projectId,
			@PathVariable("locale") String locale) {
		List<Glossary> glossaries = glossaryService
				.getGlossariesForProjectIdAndLanguage(projectId, locale);
		return glossaries.isEmpty() ?
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
						"Not Glossaries of locale(" + locale + ") were not found for Project with id "
								+ projectId),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(glossaries);
	}

	@GetMapping("/project/{projectId}/translation/{version}/language/{locale}/glossary")
	@JsonView(Views.JustGlossaries.class)
	public ResponseEntity getAllGlossariesByProjectIdLanguageLocaleTranslationVersion(
			@PathVariable("projectId") Long projectId, @PathVariable("locale") String locale,
			@PathVariable("version") String version) {
		List<Glossary> glossaries = glossaryService.findByProjectIdLocaleVersion(projectId, locale, version);
		return glossaries.isEmpty() ?
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
						"Not Glossaries of locale(" + locale + ") were not found for Project with id "
								+ projectId + " And Translation Version " + version),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(glossaries);
	}

	@GetMapping("/project/{projectId}/translation/{version}/language/{locale}/glossary/map")
	public ResponseEntity getAllGlossariesAsMapByProjectIdLanguageLocaleTranslationVersion(
			@PathVariable("projectId") Long projectId,
			@PathVariable("locale") String locale, @PathVariable("version") String version) {
		List<Glossary> glossaries = glossaryService.findByProjectIdLocaleVersion(projectId, locale, version);
		Map<String, String> map = glossaries
				.parallelStream()
				.collect(Collectors.toMap(Glossary::getSourceText, Glossary::getTranslatedText));
		return map.isEmpty() ?
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
						"Not Glossaries of locale(" + locale + ") were not found for Project with id "
								+ projectId + " And Translation Version " + version),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(map);
	}

	@PostMapping("/project/{projectId}/translation/{translationVersion}/language/{locale}/glossary")
	public ResponseEntity createGlossary(@RequestBody Glossary glossary, @PathVariable("projectId") Long projectId,
			@PathVariable("locale") String locale, @PathVariable("translationVersion") String translationVersion) {
		Optional<Project> project = projectService.findById(projectId);
		if (!project.isPresent()) {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "Project with id " + projectId + " Not found"),
					HttpStatus.NOT_FOUND);
		}
		Optional<Translation> translation = translationService.findByVersion(translationVersion, projectId);
		if (!translation.isPresent()) {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "Translation version " + translationVersion + " Not found"),
					HttpStatus.NOT_FOUND);
		}
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translationVersion, locale);
		if (!language.isPresent()) {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "Language locale " + locale + " Not found"),
					HttpStatus.NOT_FOUND);
		}
		glossary.setProject(project.get());
		glossary.setTranslation(translation.get());
		glossary.setLanguage(language.get());
		glossaryService.saveAndFlush(glossary);
		return new ResponseEntity(new ApiResponse(HttpStatus.CREATED,
				"Glossary created for Language " + locale + " in Translation version " + translationVersion
						+ "'s , Project(" + projectId + ")"), HttpStatus.CREATED);
	}

	@PutMapping("/project/{projectId}/translation/{translationVersion}/language/{locale}/glossary")
	public ResponseEntity updateGlossary(@RequestBody Glossary glossary, @PathVariable("projectId") Long projectId,
			@PathVariable("locale") String locale, @PathVariable("translationVersion") String translationVersion) {
		Optional<Project> project = projectService.findById(projectId);
		if (!project.isPresent()) {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "Project with id " + projectId + " Not found"),
					HttpStatus.NOT_FOUND);
		}
		Optional<Translation> translation = translationService.findByVersion(translationVersion, projectId);
		if (!translation.isPresent()) {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "Translation version " + translationVersion + " Not found"),
					HttpStatus.NOT_FOUND);
		}
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translationVersion, locale);
		if (!language.isPresent()) {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "Language locale " + locale + " Not found"),
					HttpStatus.NOT_FOUND);
		}
		Glossary existingGlossary = glossaryService
				.findByProjectIdLocaleVersionSourceText(projectId, locale, translationVersion,
						glossary.getSourceText());
		if (existingGlossary != null) {
			Glossary newGlossary = new Glossary();
			newGlossary.setProject(project.get());
			newGlossary.setTranslation(translation.get());
			newGlossary.setLanguage(language.get());
			newGlossary.setSourceText(glossary.getSourceText());
			newGlossary.setTranslatedText(glossary.getTranslatedText());
			newGlossary.setId(existingGlossary.getId());
			glossaryService.saveAndFlush(newGlossary);
			return new ResponseEntity(new ApiResponse(HttpStatus.OK,
					"Glossary Updated for Language " + locale + " in Translation version " + translationVersion
							+ "'s , Project(" + projectId + ")"), HttpStatus.OK);
		} else {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
					"Glossary Source Text(" + glossary.getSourceText() + ") not found for Language " + locale
							+ " in Translation version " + translationVersion
							+ "'s , Project(" + projectId + ")"), HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/project/{projectId}/translation/{translationVersion}/language/{locale}/glossary/file")
	public ResponseEntity createGlossariesFromFile(@RequestParam("file") MultipartFile file,
			@RequestParam("lineSeparator") String lineSeparator, @PathVariable("projectId") Long projectId,
			@PathVariable("locale") String locale, @PathVariable("translationVersion") String translationVersion)
			throws IOException {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(translationVersion, projectId);
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translationVersion, locale);
		if (file != null && project.isPresent() && translation.isPresent() && language.isPresent()) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
			String line;
			Set<Glossary> glossaries = new HashSet();
			while ((line = reader.readLine()) != null) {
				String[] readLine = line.split(lineSeparator);
				if (readLine.length == 2) {
					Glossary glossary = new Glossary(readLine[0], readLine[1]);
					glossary.setProject(project.get());
					glossary.setTranslation(translation.get());
					glossary.setLanguage(language.get());
					glossaries.add(glossary);
				} else {
					logger.info(line + " is invalid");
				}
			}
			if (!glossaries.isEmpty()) {
				List<Glossary> glossaryList = glossaryService.save(glossaries);
				return new ResponseEntity(new ApiResponse(HttpStatus.CREATED,
						FormString.from("Successfully created ", String.valueOf(glossaryList.size()),
								" Glossaries for Locale ", locale, ", Translation version ", translationVersion,
								" in Project ", project.get().getName())), HttpStatus.CREATED);
			} else {
				return new ResponseEntity(new ApiResponse(HttpStatus.NO_CONTENT,
						"Given file does not contain valid Glossaries"), HttpStatus.NO_CONTENT);
			}
		}
		if (file == null) {
			return new ResponseEntity(new ApiResponse(HttpStatus.NO_CONTENT,
					"Given file is not valid"), HttpStatus.NO_CONTENT);
		} else if (!project.isPresent()) {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
					FormString.from("Project with id ", String.valueOf(projectId), " not found")),
					HttpStatus.NOT_FOUND);
		} else if (!translation.isPresent()) {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
					FormString.from("Translation with version ", translationVersion, " not found")),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
					FormString.from("Language with Locale ", locale, " not found")),
					HttpStatus.NOT_FOUND);
		}
	}

}