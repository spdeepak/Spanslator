package com.github.spanslator.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;
import com.github.spanslator.entity.Project;
import com.github.spanslator.service.ProjectService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@RestController
@RequestMapping("/api")
@SuppressWarnings("unchecked")
@Api(tags = { "Project" })
public class ProjectApiController {

	private static final Logger logger = LoggerFactory.getLogger(ProjectApiController.class);

	@Autowired
	private ProjectService projectService;

	@GetMapping("/projects")
	@JsonView(Views.JustProjects.class)
	public ResponseEntity getProjects() {
		List<Project> projects = projectService.getAllProjects();
		logger.info("Total number of projects found: " + projects.size());
		return projects.isEmpty() ?
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND, "No projects were found"),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(projects);
	}

	@GetMapping("/project/{id}")
	@JsonView(Views.JustProjects.class)
	public ResponseEntity getProjectById(@PathVariable("id") Long id) {
		Optional<Project> project = projectService.findById(id);
		logger.info("Project with id(" + id + ") found? " + project.isPresent());
		if (project.isPresent()) {
			return ResponseEntity.ok(project.get());
		}
		return new ResponseEntity(
				new ApiResponse(HttpStatus.NOT_FOUND, "Project with id(" + id + ") was not found"),
				HttpStatus.NOT_FOUND);
	}

	@PostMapping(value = "/project")
	public ResponseEntity createProject(@RequestBody Project project) {
		if (projectService.isValid(project)) {
			projectService.saveAndFlush(project);
			return new ResponseEntity(
					new ApiResponse(HttpStatus.CREATED, project.getName() + " is created with id " + project.getId()),
					HttpStatus.CREATED);
		} else if (project.getName() == null || project.getName().trim().isEmpty()) {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_ACCEPTABLE, "Project name is Mandatory"),
					HttpStatus.NOT_ACCEPTABLE);
		} else {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_ACCEPTABLE,
					"Project with name (" + project.getName() + ") already exists"),
					HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@PutMapping("/project/{id}")
	public ResponseEntity updateProject(@PathVariable("id") Long id, @RequestBody Project project) {
		Optional<Project> idProject = projectService.findById(id);
		if (project.getId() == null && idProject.isPresent()) {
			Project updateProject = idProject.get();
			if (project.getName() != null && !project.getName().trim().isEmpty()) {
				updateProject.setName(project.getName());
			} else {
				return new ResponseEntity(
						new ApiResponse(HttpStatus.FORBIDDEN,
								"Updated project name cannot be " + (project.getName() != null ? "empty" : "Null")),
						HttpStatus.FORBIDDEN);
			}
			if (project.getDescription() != null) {
				updateProject.setDescription(project.getDescription());
			} else {
				return new ResponseEntity(
						new ApiResponse(HttpStatus.FORBIDDEN, "Description can be empty bit not null"),
						HttpStatus.FORBIDDEN);
			}
			updateProject.setId(id);
			updateProject.setCreatedDate(project.getCreatedDate());
			projectService.save(updateProject);
			return new ResponseEntity(new ApiResponse(HttpStatus.OK,
					"Project Updated, CreatedDate retained the same as during Project Creation"),
					HttpStatus.OK);
		} else if (project.getId() != null) {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.FORBIDDEN, "Cannot change the ID of a Project"),
					HttpStatus.FORBIDDEN);
		} else {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "Project with ID (" + id + ") not found"),
					HttpStatus.NOT_FOUND);
		}
	}

}
