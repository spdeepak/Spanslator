package com.github.spanslator.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;
import com.github.spanslator.entity.Language;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import com.github.spanslator.service.LanguageService;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.TranslationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@RestController
@RequestMapping("/api")
@SuppressWarnings("unchecked")
@Api(tags = { "Language" })
public class LanguageApiController {

	private static final String LANGUAGE_NAME_MANDATORY = "Language name is mandatory";

	private static final String LANGUAGE_LOCALE_MANDATORY = "Language Locale is mandatory";

	private static final String LOCALE_NAME_MANDATORY = "Language Name and Locale is Mandatory";

	private static final String VALID = "Valid";

	@Autowired
	private LanguageService languageService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TranslationService translationService;

	@GetMapping("/project/{projectId}/translation/{translationsVersion}/languages")
	@JsonView(Views.JustLanguages.class)
	public ResponseEntity getAllLanguagesForProject(@PathVariable("projectId") Long projectId,
			@PathVariable("translationsVersion") String translationsVersion) {
		List<Language> languages = languageService
				.getLanguagesByProjectIdAndTranslationVersion(projectId, translationsVersion);
		return languages.isEmpty() ?
				new ResponseEntity(
						new ApiResponse(HttpStatus.NOT_FOUND, "No JustLanguages were found for this project"),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(languages);
	}

	@GetMapping("/project/{projectId}/translation/{translationsVersion}/language/enabled")
	@JsonView(Views.JustLanguagesExceptEnabledField.class)
	public ResponseEntity getEnabledLanguagesForProject(@PathVariable("projectId") Long projectId,
			@PathVariable("translationsVersion") String translationsVersion) {
		List<Language> languages = languageService
				.getEnabledByProjectIdAndTranslationVersion(projectId, translationsVersion);
		return languages.isEmpty() ?
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
						"No JustLanguages are enabled for the project(" + projectId + ") and JustTranslations Version("
								+ translationsVersion + ")"),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(languages);
	}

	@GetMapping("/project/{projectId}/translation/{translationsVersion}/language/disabled")
	@JsonView(Views.JustLanguagesExceptEnabledField.class)
	public ResponseEntity getDisabledLanguagesForProject(@PathVariable("projectId") Long projectId,
			@PathVariable("translationsVersion") String translationsVersion) {
		List<Language> languages = languageService
				.getDisabledByProjectIdAndTranslationVersion(projectId, translationsVersion);
		return languages.isEmpty() ?
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
						"No Languages are disabled for the project(" + projectId + ") and JustTranslations Version("
								+ translationsVersion + ")"),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(languages);
	}

	@PostMapping("/project/{projectId}/translation/{translationsVersion}/language")
	public ResponseEntity createLanguage(@PathVariable("projectId") Long projectId,
			@PathVariable("translationsVersion") String translationsVersion, @RequestBody Language language) {
		Optional<Project> project = projectService.findById(projectId);
		if (project.isPresent()) {
			Optional<Translation> translations = translationService
					.findByVersion(translationsVersion, project.get().getId());
			if (translations.isPresent()) {
				language.setProject(project.get());
				language.setTranslation(translations.get());
				String validMessage = validateLanguage(language);
				return validateLanguageAndRespond(language, validMessage);
			} else {
				return new ResponseEntity(
						new ApiResponse(HttpStatus.NOT_FOUND,
								"No JustTranslations with version (" + translationsVersion
										+ ") found in Project with ID: "
										+ projectId),
						HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity(
					new ApiResponse(HttpStatus.NOT_FOUND, "No Project found with ID: " + projectId),
					HttpStatus.NOT_FOUND);
		}
	}

	private ResponseEntity validateLanguageAndRespond(@RequestBody Language language, String validMessage) {
		if (validMessage.equals(VALID)) {
			languageService.saveAndFlush(language);
			return new ResponseEntity(new ApiResponse(HttpStatus.CREATED,
					"Language with name(" + language.getName() + ") and locale(" + language.getLocale()
							+ ") is created"), HttpStatus.CREATED);
		} else {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_ACCEPTABLE, validMessage),
					HttpStatus.NOT_ACCEPTABLE);
		}
	}

	public String validateLanguage(Language language) {
		if (language.getName() == null || language.getName().trim().isEmpty()) {
			if (language.getLocale() == null || language.getLocale().trim().isEmpty()) {
				return LOCALE_NAME_MANDATORY;
			} else {
				return LANGUAGE_NAME_MANDATORY;
			}
		} else if (language.getLocale() == null || language.getLocale().trim().isEmpty()) {
			return LANGUAGE_LOCALE_MANDATORY;
		} else {
			return VALID;
		}
	}
}
