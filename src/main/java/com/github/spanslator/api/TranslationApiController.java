package com.github.spanslator.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.TranslationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@RestController
@RequestMapping("/api")
@SuppressWarnings("unchecked")
@Api(tags = { "Translation" })
public class TranslationApiController {

	@Autowired
	private TranslationService translationService;

	@Autowired
	private ProjectService projectService;

	@GetMapping("/project/{projectId}/translations")
	@JsonView(Views.JustTranslations.class)
	public ResponseEntity<List<Translation>> getAllTranslations(
			@PathVariable("projectId") Long projectId) {
		List<Translation> translations = translationService
				.findAllByProjectId(projectId);
		return translations.isEmpty() ?
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
						"No translations were found for Project with ID: " + projectId),
						HttpStatus.NOT_FOUND) :
				ResponseEntity.ok(translations);
	}

	@GetMapping("/project/{projectId}/translation/{version}/v")
	@JsonView(Views.JustTranslations.class)
	public ResponseEntity<Translation> getTranslationsByVersion(@PathVariable("version") String version,
			@PathVariable("projectId") Long projectId) {
		Optional<Translation> translations = translationService
				.findByVersion(version, projectId);
		return translations.isPresent() ?
				ResponseEntity.ok(translations.get()) :
				new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
						"No translations were found for Project with id " + projectId
								+ " and Translation version "
								+ version), HttpStatus.NOT_FOUND);
	}

	@PostMapping("/project/{projectId}/translation/{version}/v")
	public ResponseEntity createTranslationsForProject(@PathVariable("projectId") Long projectId,
			@PathVariable("version") String version) {
		Optional<Project> project = projectService.findById(projectId);
		if (project.isPresent()) {
			Optional<Translation> translation = translationService
					.findByVersion(version, projectId);
			if (!translation.isPresent()) {
				Translation translations = new Translation(
						version);
				translations.setProject(project.get());
				translationService.saveAndFlush(translations);
				return new ResponseEntity(new ApiResponse(HttpStatus.CREATED,
						"Translation version " + version + " is created for project with id " + projectId),
						HttpStatus.CREATED);
			} else {
				return new ResponseEntity(new ApiResponse(HttpStatus.NOT_ACCEPTABLE,
						project.get().getName() + " already has a Translation of version " + version),
						HttpStatus.NOT_ACCEPTABLE);
			}
		} else {
			return new ResponseEntity(new ApiResponse(HttpStatus.NOT_FOUND,
					"Project Not Found with id ".concat(projectId.toString())), HttpStatus.NOT_FOUND);
		}
	}
}
