package com.github.spanslator.util;

/**
 * @author Deepak Sunanda Prabhakar
 */
public class FormString {

	public static String from(String... strings) {
		StringBuilder builder = new StringBuilder();
		for (String string : strings) {
			builder.append(string);
		}
		return builder.toString();
	}
}
