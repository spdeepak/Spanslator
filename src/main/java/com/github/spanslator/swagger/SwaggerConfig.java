package com.github.spanslator.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.github.spanslator.api"))
				.paths(PathSelectors.any())//works with .paths(PathSelectors.ant("/api/**"))
				.build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Spanslator")
				.description("Web Translator Engine")
				.version("1.0")
				.contact(new Contact("Deepak", "http://spdeepak.github.io", "speedpak1991@gmail.com"))
				.license("Apache 2.0")
				.licenseUrl("https://raw.githubusercontent.com/spdeepak/Spanslator/master/LICENSE")
				.build();
	}
}
