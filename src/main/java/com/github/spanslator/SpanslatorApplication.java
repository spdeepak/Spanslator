package com.github.spanslator;

import com.github.spanslator.entity.Project;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Deepak Sunanda Prabhakar
 */
@SpringBootApplication
@EnableCaching
public class SpanslatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpanslatorApplication.class, args);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public JedisConnectionFactory jedisConnectionFactory() {
		return new JedisConnectionFactory();
	}

	@Bean
	public RedisTemplate<Long, Project> projectRedisTemplate() {
		RedisTemplate<Long, Project> projectRedisTemplate = new RedisTemplate<>();
		projectRedisTemplate.setConnectionFactory(jedisConnectionFactory());
		return projectRedisTemplate;
	}
}
