package com.github.spanslator.ui;

import com.github.spanslator.entity.Language;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import com.github.spanslator.service.LanguageService;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.TranslationService;
import com.github.spanslator.util.FormString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Controller
public class LanguageUIController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TranslationService translationService;

	@Autowired
	private LanguageService languageService;

	@GetMapping("/project/{projectId}/translation/{version}/languages")
	public String gotoLanguageListPage(@PathVariable("projectId") Long projectId,
			@PathVariable("version") String version, Model model, HttpServletRequest request) {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(version, projectId);
		if (project.isPresent() && translation.isPresent()) {
			List<Language> languages = languageService
					.getLanguagesByProjectIdAndTranslationVersion(projectId, translation.get().getVersion());
			if (!languages.isEmpty()) {
				model.addAttribute("languagePresent", true);
				model.addAttribute("languages", languages);
			} else {
				model.addAttribute("languagePresent", false);
			}

		} else {
			model.addAttribute("languagePresent", false);
		}
		model.addAttribute("project", project.get());
		model.addAttribute("translation", translation.get());
		Language language = new Language();
		language.setProject(project.get());
		language.setTranslation(translation.get());
		model.addAttribute("language", language);
		return "language";
	}

	@GetMapping("/project/{projectId}/translation/{version}/language/create")
	public String createLanguageForm(@PathVariable("projectId") Long projectId,
			@PathVariable("version") String version, Model model) {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(version, projectId);
		if (project.isPresent()) {
			if (translation.isPresent()) {
				Language language = new Language();
				language.setProject(project.get());
				language.setTranslation(translation.get());
				model.addAttribute("language", language);
				//model.addAttribute("project", project.get());
				model.addAttribute("projectId", projectId);
				//model.addAttribute("translation", translation.get());
				model.addAttribute("version", translation.get().getVersion());
			} else {
				model.addAttribute("translationAbsent", true);
			}
		} else {
			model.addAttribute("projectAbsent", true);
		}
		return "createLanguage";
	}

	@PostMapping("/project/{projectId}/translation/{version}/language/create")
	public String createLanguageSubmit(@PathVariable("projectId") Long projectId,
			@PathVariable("version") String version, @ModelAttribute("language") @Valid Language language,
			Model model, BindingResult bindingResult) {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(version, projectId);
		Optional<Language> languageAlready = languageService
				.getLanguageByProjectIdVersionLocale(projectId, version, language.getLocale());

		if (project.isPresent() && translation.isPresent() && !languageAlready.isPresent()) {
			language.setProject(project.get());
			language.setTranslation(translation.get());
			languageService.saveAndFlush(language);
			String url = FormString
					.from("redirect:/project/", String.valueOf(projectId), "/translation/", version, "/languages");
			return url;
		}
		if (!project.isPresent()) {
			FieldError fieldError = new FieldError("language", "project",
					FormString.from("Project with id ", String.valueOf(projectId), " is not found"));
			bindingResult.addError(fieldError);
		} else if (!translation.isPresent()) {
			FieldError fieldError = new FieldError("language", "translation",
					FormString.from("Translation with version ", version, " is not available"));
			bindingResult.addError(fieldError);
		} else if (languageAlready.isPresent()) {
			FieldError fieldError = new FieldError("language", "locale",
					FormString.from("Language with locale ", language.getLocale(),
							" is already present for the Project (", project.get().getName(),
							") in Translation v", translation.get().getVersion()));
			bindingResult.addError(fieldError);
		}
		return "createLanguage";
	}
}
