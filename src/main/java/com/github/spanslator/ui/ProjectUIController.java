package com.github.spanslator.ui;

import com.github.spanslator.entity.Project;
import com.github.spanslator.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Controller
public class ProjectUIController {

	@Autowired
	private ProjectService projectService;

	@GetMapping("/project/create")
	public String createProjectForm(Model model) {
		model.addAttribute("project", new Project());
		return "createProject";
	}

	@PostMapping("/project/create")
	public String createProjectSubmit(@ModelAttribute("project") @Valid Project project, Model model,
			BindingResult bindingResult) {
		Project existing = projectService.findByName(project.getName());
		if (existing != null) {
			FieldError fieldError = new FieldError("project", "name",
					"Project with name ".concat(project.getName().concat(" already exists")));
			bindingResult.addError(fieldError);
			return "createProject";
		} else {
			projectService.saveAndFlush(project);
			return "redirect:/";
		}
	}

}
