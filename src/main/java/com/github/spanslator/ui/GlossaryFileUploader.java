package com.github.spanslator.ui;

import org.springframework.web.multipart.MultipartFile;

public class GlossaryFileUploader {

	private MultipartFile multipartFile;

	private String lineSeparator;

	public GlossaryFileUploader(MultipartFile multipartFile, String lineSeparator) {
		this.multipartFile = multipartFile;
		this.lineSeparator = lineSeparator;
	}

	public GlossaryFileUploader() {
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public String getLineSeparator() {
		return lineSeparator;
	}

	public void setLineSeparator(String lineSeparator) {
		this.lineSeparator = lineSeparator;
	}
}
