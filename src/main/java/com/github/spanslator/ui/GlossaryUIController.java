package com.github.spanslator.ui;

import com.github.spanslator.entity.Glossary;
import com.github.spanslator.entity.Language;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import com.github.spanslator.service.GlossaryService;
import com.github.spanslator.service.LanguageService;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.TranslationService;
import com.github.spanslator.util.FormString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Controller
public class GlossaryUIController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TranslationService translationService;

	@Autowired
	private LanguageService languageService;

	@Autowired
	private GlossaryService glossaryService;

	@GetMapping("/project/{projectId}/translation/{version}/language/{locale}/glossary")
	public String gotoGlossaryPage(@PathVariable("projectId") Long projectId, @PathVariable("version") String version,
			@PathVariable("locale") String locale, Model model, HttpServletRequest request) {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(version, projectId);
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translation.get().getVersion(), locale);
		List<Glossary> glossaries = glossaryService
				.findByProjectIdLocaleVersion(projectId, locale, translation.get().getVersion());
		Glossary glossary = new Glossary();
		glossary.setLanguage(language.get());
		glossary.setTranslation(translation.get());
		glossary.setProject(project.get());
		model.addAttribute("glossary", glossary);
		model.addAttribute("glossaryFileUploader", new GlossaryFileUploader());
		if (project.isPresent() && translation.isPresent() && language.isPresent()) {
			model.addAttribute("glossariesPresent", true);
			model.addAttribute("project", project.get());
			model.addAttribute("translation", translation.get());
			model.addAttribute("language", language.get());
			model.addAttribute("glossaries", glossaries);
		} else {
			model.addAttribute("glossariesPresent", false);
		}
		return "glossary";
	}

	@GetMapping("/project/{projectId}/translation/{version}/language/{locale}/glossary/create")
	public String createGlossaryForm(@PathVariable("projectId") Long projectId, @PathVariable("version") String version,
			@PathVariable("locale") String locale, Model model) {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(version, projectId);
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translation.get().getVersion(), locale);
		if (project.isPresent() && translation.isPresent() && language.isPresent()) {
			Glossary glossary = new Glossary();
			glossary.setLanguage(language.get());
			glossary.setTranslation(translation.get());
			glossary.setProject(project.get());
			model.addAttribute("glossary", glossary);
			model.addAttribute("projectId", project.get().getId());
			model.addAttribute("version", translation.get().getVersion());
			model.addAttribute("locale", language.get().getLocale());
		}
		return "createGlossary";
	}

	@PostMapping("/project/{projectId}/translation/{version}/language/{locale}/glossary/create")
	public String createGlossarySubmit(@PathVariable("projectId") Long projectId,
			@PathVariable("version") String version, @PathVariable("locale") String locale,
			@ModelAttribute("glossary") Glossary glossary, BindingResult bindingResult) {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(version, projectId);
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translation.get().getVersion(), locale);
		Glossary existingGlossary = glossaryService
				.findByProjectIdLocaleVersionSourceText(projectId, locale, translation.get().getVersion(),
						glossary.getSourceText());
		if (project.isPresent() && translation.isPresent() && language.isPresent() && existingGlossary == null) {
			glossary.setProject(project.get());
			glossary.setLanguage(language.get());
			glossary.setTranslation(translation.get());
			glossaryService.saveAndFlush(glossary);
			String url = FormString
					.from("redirect:/project/", String.valueOf(projectId), "/translation/", version, "/language/",
							locale,
							"/glossary");
			return url;
		} else {
			if (!project.isPresent()) {
				FieldError fieldError = new FieldError("glossary", "project",
						FormString.from("Project with id ", String.valueOf(projectId), " is not found"));
				bindingResult.addError(fieldError);
			}
			if (!translation.isPresent()) {
				FieldError fieldError = new FieldError("glossary", "translation",
						FormString.from("Translation with version ", version, " is not available"));
				bindingResult.addError(fieldError);
			}
			if (!language.isPresent()) {
				FieldError fieldError = new FieldError("glossary", "language",
						FormString.from("Language with locale ", language.get().getLocale(),
								" is already present for the Project (", project.get().getName(),
								") in Translation v", translation.get().getVersion()));
				bindingResult.addError(fieldError);
			}
			if (existingGlossary != null) {
				FieldError error = new FieldError("glossary", "sourceText", FormString
						.from("Glossary with Source Text(", glossary.getSourceText(),
								") is already present in the Project with id ", project.get().getId().toString(),
								", Translation with version ", translation.get().getVersion(), " and language ",
								language.get().getLocale()));
				bindingResult.addError(error);
			}
			return "createGlossary";
		}
	}

	@GetMapping("/project/{projectId}/translation/{version}/language/{locale}/glossary/createByFile")
	public String createGlossaryByFileForm(@PathVariable("projectId") Long projectId,
			@PathVariable("version") String version, @PathVariable("locale") String locale, Model model) {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(version, projectId);
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translation.get().getVersion(), locale);
		if (project.isPresent() && translation.isPresent() && language.isPresent()) {
			model.addAttribute("projectId", project.get().getId());
			model.addAttribute("version", translation.get().getVersion());
			model.addAttribute("locale", language.get().getLocale());
			model.addAttribute("glossaryFileUploader", new GlossaryFileUploader());
		}
		return "createGlossaryByFile";
	}

	@PostMapping("/project/{projectId}/translation/{translationVersion}/language/{locale}/glossary/createByFile")
	public String createGlossaryByFileSubmit(
			@ModelAttribute("glossaryFileUploader") GlossaryFileUploader glossaryFileUploader,
			@PathVariable("projectId") Long projectId, @PathVariable("locale") String locale,
			@PathVariable("translationVersion") String translationVersion,
			BindingResult bindingResult)
			throws IOException {
		Optional<Project> project = projectService.findById(projectId);
		Optional<Translation> translation = translationService.findByVersion(translationVersion, projectId);
		Optional<Language> language = languageService
				.getLanguageByProjectIdVersionLocale(projectId, translation.get().getVersion(), locale);
		if (glossaryFileUploader.getMultipartFile() != null
				&& glossaryFileUploader.getLineSeparator().trim().length() == 1 && project.isPresent() && translation
				.isPresent() && language.isPresent()) {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(glossaryFileUploader.getMultipartFile().getInputStream()));
			String line;
			Set<Glossary> glossaries = new HashSet();
			while ((line = reader.readLine()) != null) {
				String[] readLine = line.split(glossaryFileUploader.getLineSeparator());
				if (readLine.length == 2) {
					Glossary glossary = new Glossary(readLine[0], readLine[1]);
					glossary.setProject(project.get());
					glossary.setTranslation(translation.get());
					glossary.setLanguage(language.get());
					glossaries.add(glossary);
				}
			}
			if (!glossaries.isEmpty()) {
				glossaryService.save(glossaries);
			}
			String url = FormString
					.from("redirect:/project/", String.valueOf(projectId), "/translation/", translationVersion,
							"/language/",
							locale,
							"/glossary");
			return url;
		} else {
			if (!project.isPresent()) {
				FieldError fieldError = new FieldError("glossary", "project",
						FormString.from("Project with id ", String.valueOf(projectId), " is not found"));
				bindingResult.addError(fieldError);
			}
			if (!translation.isPresent()) {
				FieldError fieldError = new FieldError("glossary", "translation",
						FormString.from("Translation with version ", translationVersion, " is not available"));
				bindingResult.addError(fieldError);
			}
			if (!language.isPresent()) {
				FieldError fieldError = new FieldError("glossary", "language",
						FormString.from("Language with locale ", language.get().getLocale(),
								" is already present for the Project (", project.get().getName(),
								") in Translation v", translation.get().getVersion()));
				bindingResult.addError(fieldError);
			}
			return "createGlossaryByFile";
		}
	}
}
