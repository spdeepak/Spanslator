package com.github.spanslator.ui;

import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Controller
public class TranslationUIController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TranslationService translationService;

	@GetMapping("/project/{projectId}/translations")
	public String gotoTranslationsPage(@PathVariable("projectId") Long projectId, Model model,
			HttpServletRequest request) {
		Optional<Project> project = projectService.findById(projectId);
		List<Translation> translations = translationService.findAllByProjectId(projectId);
		if (project.isPresent()) {
			Translation translation = new Translation();
			translation.setProject(project.get());
			model.addAttribute("projectPresent", true);
			model.addAttribute("project", project.get());
			model.addAttribute("translations", translations);
			model.addAttribute("translation", translation);
		} else {
			model.addAttribute("projectPresent", false);
		}
		return "translation";
	}

	@GetMapping("/project/{projectId}/translation/create")
	public String createTranslationForm(@PathVariable("projectId") Long projectId, Model model) {
		Optional<Project> project = projectService.findById(projectId);
		if (project.isPresent()) {
			Translation translation = new Translation();
			translation.setProject(project.get());
			model.addAttribute("valid", true);
			model.addAttribute("project", project.get());
			model.addAttribute("projectId", projectId);
			model.addAttribute("translation", translation);
		} else {
			model.addAttribute("valid", false);
			model.addAttribute("projectId", projectId);
		}
		return "createTranslation";
	}

	@PostMapping("/project/{projectId}/translation/create")
	public String createTranslationSubmit(@PathVariable("projectId") Long projectId,
			@ModelAttribute("translation") @Valid Translation translation, Model model, BindingResult bindingResult) {
		Optional<Project> project = projectService.findById(projectId);
		if (project.isPresent()) {
			Optional<Translation> existingTranslation = translationService
					.findByVersion(translation.getVersion(), projectId);
			if (!existingTranslation.isPresent()) {
				translation.setProject(project.get());
				translationService.saveAndFlush(translation);
				String url = "redirect:/project/" + String.valueOf(projectId) + "/translations";
				return url;
			} else {
				FieldError error = new FieldError("translation", "version",
						"Translation with version " + translation.getVersion() + " is already present in project "
								+ project.get().getName());
				bindingResult.addError(error);
				return "createTranslation";
			}
		} else {
			FieldError error = new FieldError("translation", "project",
					"Project with id(" + projectId + ") is not present");
			bindingResult.addError(error);
			return "createTranslation";
		}
	}
}
