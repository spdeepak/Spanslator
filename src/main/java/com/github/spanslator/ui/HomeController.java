package com.github.spanslator.ui;

import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.User;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.UserService;
import com.github.spanslator.util.FormString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private UserService userService;

	@GetMapping("/")
	public String gotoHomePage(Model model) {
		model.addAttribute("projects", projectService.getAllProjects());
		model.addAttribute("project", new Project());
		return "home";
	}

	@GetMapping("/login")
	public String loginPage() {
		return "login";
	}

	@GetMapping("/register")
	public String gotoRegisterPage(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}

	@PostMapping("/register")
	public String createUserForm(@ModelAttribute("user") User user, BindingResult bindingResult) {
		try {
			userService.loadUserByUsername(user.getUsername());
			FieldError fieldError = new FieldError("user", "username",
					FormString.from("Username", user.getUsername(), " already exists"));
			bindingResult.addError(fieldError);
			return "redirect:/register";
		} catch (UsernameNotFoundException exception) {
			user.setAccountNonExpired(true);
			user.setAccountNonLocked(true);
			user.setCredentialsNonExpired(true);
			user.setEnabled(true);
			userService.save(user);
		}
		return "redirect:/login";
	}
}
