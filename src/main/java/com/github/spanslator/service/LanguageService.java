package com.github.spanslator.service;

import com.github.spanslator.entity.Language;
import com.github.spanslator.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Service
public class LanguageService {

	@Autowired
	private LanguageRepository languageRepository;

	public List<Language> getLanguagesByProjectIdAndTranslationVersion(Long projectId, String version) {
		return languageRepository.findAllByProjectIdAndTranslationVersion(projectId, version);
	}

	public List<Language> getLanguagesByProjectId(Long projectId) {
		return languageRepository.findAllByProjectId(projectId);
	}

	public List<Language> getEnabledByProjectIdAndTranslationVersion(Long projectId, String translationVersion) {
		return languageRepository
				.findAllByEnabledTrueAndProjectIdAndTranslationVersion(projectId, translationVersion);
	}

	public List<Language> getDisabledByProjectIdAndTranslationVersion(Long projectId, String translationVersion) {
		return languageRepository
				.findAllByEnabledFalseAndProjectIdAndTranslationVersion(projectId, translationVersion);
	}

	public Language getEnabledLanguageByLocaleAndProjectId(String locale, Long projectId) {
		return languageRepository.findByLocaleAndEnabledTrueAndProjectId(locale, projectId);
	}

	public Optional<Language> getLanguageByProjectIdVersionLocale(Long projectId, String version, String locale) {
		return languageRepository.findAllByProjectIdAndTranslationVersionAndLocale(projectId, version, locale);
	}

	public Language saveAndFlush(Language language) {
		return languageRepository.saveAndFlush(language);
	}

	public Language save(Language language) {
		return languageRepository.save(language);
	}

	public List<Language> saveAll(Collection<Language> language) {
		return languageRepository.saveAll(language);
	}

	public void delete(Language language) {
		languageRepository.delete(language);
	}

	public void delete(Long languageId) {
		languageRepository.deleteById(languageId);
	}

	public void deleteAll(List<Language> languages) {
		languageRepository.deleteAll(languages);
	}
}
