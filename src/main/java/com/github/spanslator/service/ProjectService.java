package com.github.spanslator.service;

import com.github.spanslator.entity.Project;
import com.github.spanslator.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Component
public class ProjectService {

	@Autowired
	private ProjectRepository projectRepository;

	public List<Project> getAllProjects() {
		return projectRepository.findAll();
	}

	public Project saveAndFlush(Project project) {
		return projectRepository.saveAndFlush(project);
	}

	public Optional<Project> findById(Long id) {
		return projectRepository.findById(id);
	}

	public Project findByName(String name) {
		return projectRepository.findByNameIgnoreCase(name);
	}

	public Project save(Project project) {
		return projectRepository.save(project);
	}

	public void delete(Long id) {
		projectRepository.deleteById(id);
	}

	public void delete(Project project) {
		projectRepository.delete(project);
	}

	public boolean isValid(Project project) {
		if (project.getName() != null && !project.getName().trim().isEmpty()) {
			return findByName(project.getName()) == null;
		}
		return false;
	}
}
