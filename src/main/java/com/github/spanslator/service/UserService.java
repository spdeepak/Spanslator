package com.github.spanslator.service;

import com.github.spanslator.entity.User;
import com.github.spanslator.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> optionalUser = userRepository.findByUsername(username);
		return optionalUser.orElseThrow(() -> new UsernameNotFoundException("Username(" + username + ") not found"));
	}

	public User findByUsernameOrEmail(String username, String email) {
		Optional<User> optionalUser = userRepository.findByUsernameOrEmail(username, email);
		return optionalUser.orElseThrow(() -> new UsernameNotFoundException("Username(" + username + ") not found"));
	}

	public User save(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return userRepository.save(user);
	}
}
