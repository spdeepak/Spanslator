package com.github.spanslator.service;

import com.github.spanslator.entity.Translation;
import com.github.spanslator.repository.TranslationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Service
public class TranslationService {

	@Autowired
	private TranslationRepository translationRepository;

	public Translation saveAndFlush(Translation translation) {
		return translationRepository.saveAndFlush(translation);
	}

	public Optional<Translation> findById(Long id) {
		return translationRepository.findById(id);
	}

	public List<Translation> saveAll(Collection<Translation> translations) {
		return translationRepository.saveAll(translations);
	}

	public List<Translation> findAllByProjectId(Long projectId) {
		return translationRepository.findAllByProjectId(projectId);
	}

	public Optional<Translation> findByVersion(String version, Long projectId) {
		return translationRepository.findByVersionAndProjectId(version, projectId);
	}

	public void deleteAll(Collection<Translation> translations) {
		translationRepository.deleteAll(translations);
	}

	public void delete(Translation translation) {
		translationRepository.delete(translation);
	}

	public void deleteById(Long translationsId) {
		translationRepository.deleteById(translationsId);
	}
}
