package com.github.spanslator.service;

import com.github.spanslator.entity.Glossary;
import com.github.spanslator.repository.GlossaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Service
public class GlossaryService {

	@Autowired
	private GlossaryRepository glossaryRepository;

	public List<Glossary> getGlossariesForProjectIdAndLanguage(Long projectId, String locale) {
		return glossaryRepository.findAllByProjectIdAndLanguageLocaleAndLanguageEnabledTrue(projectId, locale);
	}

	public Glossary saveAndFlush(Glossary glossary) {
		return glossaryRepository.saveAndFlush(glossary);
	}

	public Glossary findByProjectIdLocaleSourceText(Long projectId, String locale, String sourceText) {
		return glossaryRepository.findByProjectIdLocaleSourceText(projectId, locale, sourceText);
	}

	public List<Glossary> findByProjectIdLocaleVersion(Long projectId, String locale, String translationVersion) {
		return glossaryRepository.findByProjectIdLocaleVersion(projectId, locale, translationVersion);
	}

	public Glossary findByProjectIdLocaleVersionSourceText(Long projectId, String locale,
			String translationVersion, String sourceText) {
		return glossaryRepository
				.findByProjectIdLocaleVersionSourceText(projectId, locale, translationVersion, sourceText);
	}
	public Glossary save(Glossary glossary) {
		return glossaryRepository.save(glossary);
	}

	public List<Glossary> save(Collection<Glossary> glossaries) {
		return glossaryRepository.saveAll(glossaries);
	}

	public void delete(Glossary glossary) {
		glossaryRepository.delete(glossary);
	}

	public void delete(Long glossaryId) {
		glossaryRepository.deleteById(glossaryId);
	}

	public void deleteAll(List<Glossary> glossaries) {
		glossaryRepository.deleteAll(glossaries);
	}

	public boolean validateGlossaryBeforeSaving(Long projectId, String locale, String version, Glossary glossary) {
		if (glossary.getSourceText() != null) {
			return findByProjectIdLocaleVersionSourceText(projectId, locale, version,
					glossary.getSourceText()) != null;
		} else {
			return false;
		}
	}
}
