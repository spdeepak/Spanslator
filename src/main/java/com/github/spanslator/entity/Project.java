package com.github.spanslator.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Entity
public class Project implements Serializable {

	@Id
	@GeneratedValue
	@JsonView(Views.JustProjects.class)
	private Long id;

	@Column(unique = true)
	@JsonView(Views.JustProjects.class)
	@NotNull
	private String name;

	@JsonView(Views.JustProjects.class)
	private String description;

	@Temporal(TemporalType.DATE)
	@JsonFormat(
			shape = JsonFormat.Shape.STRING,
			pattern = "dd-MMMM-yyyy")
	@JsonView(Views.JustProjects.class)
	private Date createdDate = new Date();

	public Project() {
	}

	public Project(String name) {
		this.name = name;
	}

	public Project(@NotNull String name, @NotNull String description) {
		this.name = name;
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
