package com.github.spanslator.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Entity
public class Translation {

	@Id
	@GeneratedValue
	@JsonView(Views.JustTranslations.class)
	private Long id;

	@NotNull
	@JsonView(Views.JustTranslations.class)
	private String version;

	@OneToOne(fetch = FetchType.EAGER)
	private Project project;

	public Translation() {
	}

	public Translation(String version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
}
