package com.github.spanslator.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Entity
public class Language {

	@Id
	@GeneratedValue
	@JsonView({ Views.JustLanguages.class, Views.JustLanguagesExceptEnabledField.class })
	private Long id;

	@NotNull
	@JsonView({ Views.JustLanguages.class, Views.JustLanguagesExceptEnabledField.class })
	private String name;

	@NotNull
	@JsonView({ Views.JustLanguages.class, Views.JustLanguagesExceptEnabledField.class })
	private String locale;

	@JsonView(Views.JustLanguages.class)
	private boolean enabled;

	@OneToOne(fetch = FetchType.EAGER)
	private Project project;

	@OneToOne(fetch = FetchType.EAGER)
	private Translation translation;

	public Language() {
	}

	public Language(String name, String locale) {
		this.locale = locale;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Translation getTranslation() {
		return translation;
	}

	public void setTranslation(Translation translation) {
		this.translation = translation;
	}
}
