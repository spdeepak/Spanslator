package com.github.spanslator.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.github.spanslator.api.json.views.Views;

import javax.persistence.*;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Entity
public class Glossary {

	@Id
	@GeneratedValue
	@JsonView(Views.JustGlossaries.class)
	private Long id;

	@JsonView(Views.JustGlossaries.class)
	private String sourceText;

	@JsonView(Views.JustGlossaries.class)
	private String translatedText;

	@OneToOne(fetch = FetchType.EAGER)
	private Language language;

	@OneToOne(fetch = FetchType.EAGER)
	private Project project;

	@OneToOne(fetch = FetchType.EAGER)
	private Translation translation;

	public Glossary() {
	}

	public Glossary(String sourceText, String translatedText) {
		this.sourceText = sourceText;
		this.translatedText = translatedText;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSourceText() {
		return sourceText;
	}

	public void setSourceText(String sourceText) {
		this.sourceText = sourceText;
	}

	public String getTranslatedText() {
		return translatedText;
	}

	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Translation getTranslation() {
		return translation;
	}

	public void setTranslation(Translation translation) {
		this.translation = translation;
	}

	@Override
	public String toString() {
		return "Glossary{" +
				"id=" + id +
				", sourceText='" + sourceText + '\'' +
				", translatedText='" + translatedText + '\'' +
				", language=" + language +
				", project=" + getProject().getId() +
				", translation=" + getTranslation().getId() +
				'}';
	}
}
