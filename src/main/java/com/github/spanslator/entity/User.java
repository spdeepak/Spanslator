package com.github.spanslator.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Entity
public class User implements UserDetails {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	@NotEmpty
	private String username;

	private String firstname;

	private String lastname;

	@Email
	@NotEmpty
	private String email;

	@Temporal(TemporalType.DATE)
	@JsonFormat(
			shape = JsonFormat.Shape.STRING,
			pattern = "dd-MMMM-yyyy")
	private Date createdDate = new Date();

	private String password;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Project> projects = new HashSet();

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean accountNonExpired;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean accountNonLocked;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean credentialsNonExpired;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean enabled;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Authority> authorities = new HashSet();

	public User() {
	}

	public User(User user) {
		id = user.getId();
		username = user.getUsername();
		email = user.getEmail();
		createdDate = user.getCreatedDate();
		projects = user.getProjects();
		authorities = user.getAuthorities();
		password = user.getPassword();
		firstname = user.getFirstname();
		lastname = user.getLastname();
		accountNonExpired = user.isAccountNonExpired();
		accountNonLocked = user.isAccountNonLocked();
		credentialsNonExpired = user.isCredentialsNonExpired();
		enabled = user.isEnabled();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}
}
