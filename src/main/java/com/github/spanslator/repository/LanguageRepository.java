package com.github.spanslator.repository;

import com.github.spanslator.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Repository
public interface LanguageRepository extends JpaRepository<Language, Long> {

	List<Language> findAllByProjectIdAndTranslationVersion(Long projectId, String version);

	List<Language> findAllByEnabledTrueAndProjectIdAndTranslationVersion(Long projectId, String version);

	List<Language> findAllByEnabledFalseAndProjectIdAndTranslationVersion(Long projectId, String version);

	Language findByLocaleAndEnabledTrueAndProjectId(String locale, Long projectId);

	List<Language> findAllByProjectId(Long projectId);

	Optional<Language> findAllByProjectIdAndTranslationVersionAndLocale(Long projectId, String translationsVersion,
			String locale);
}
