package com.github.spanslator.repository;

import com.github.spanslator.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	Optional<User> findByUsernameAndEmail(String username, String email);

	Optional<User> findByUsernameOrEmail(String username, String email);
}
