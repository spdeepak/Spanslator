package com.github.spanslator.repository;

import com.github.spanslator.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

	Project findByNameIgnoreCase(String name);

}
