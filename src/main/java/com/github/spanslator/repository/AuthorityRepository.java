package com.github.spanslator.repository;

import com.github.spanslator.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

}
