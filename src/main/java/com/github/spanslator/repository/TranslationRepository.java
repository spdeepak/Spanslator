package com.github.spanslator.repository;

import com.github.spanslator.entity.Translation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Repository
public interface TranslationRepository extends JpaRepository<Translation, Long> {

	Optional<Translation> findByVersionAndProjectId(String version, Long projectId);

	List<Translation> findAllByProjectId(Long projectId);
}
