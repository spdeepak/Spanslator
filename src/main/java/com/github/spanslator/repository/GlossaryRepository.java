package com.github.spanslator.repository;

import com.github.spanslator.entity.Glossary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Deepak Sunanda Prabhakar
 */
@Repository
public interface GlossaryRepository extends JpaRepository<Glossary, Long> {

	List<Glossary> findAllByProjectIdAndLanguageLocaleAndLanguageEnabledTrue(Long projectId, String locale);

	Glossary findByProjectIdAndLanguageLocaleAndLanguageEnabledTrueAndSourceText(Long projectId, String locale,
			String sourceText);

	List<Glossary> findByProjectIdAndLanguageLocaleAndTranslationVersionAndLanguageEnabledTrue(Long projectId,
			String locale, String translationsVersion);

	Glossary findByProjectIdAndLanguageLocaleAndTranslationVersionAndLanguageEnabledTrueAndSourceText(Long projectId,
			String locale, String translationsVersion, String sourceText);

	default Glossary findByProjectIdLocaleSourceText(Long projectId, String locale, String sourceText) {
		return findByProjectIdAndLanguageLocaleAndLanguageEnabledTrueAndSourceText(projectId, locale, sourceText);
	}

	default List<Glossary> findByProjectIdLocaleVersion(Long projectId, String locale, String translationsVersion) {
		return findByProjectIdAndLanguageLocaleAndTranslationVersionAndLanguageEnabledTrue(projectId, locale,
				translationsVersion);
	}

	default Glossary findByProjectIdLocaleVersionSourceText(Long projectId,
			String locale, String translationsVersion, String sourceText) {
		return findByProjectIdAndLanguageLocaleAndTranslationVersionAndLanguageEnabledTrueAndSourceText(projectId,
				locale, translationsVersion, sourceText);
	}

}
