CREATE TABLE Authority (
  id        BIGINT NOT NULL,
  authority VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE Glossary (
  id             BIGINT NOT NULL,
  sourceText     VARCHAR(255),
  translatedText VARCHAR(255),
  language_id    BIGINT,
  project_id     BIGINT,
  translation_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE hibernate_sequence (
  next_val BIGINT
);
INSERT INTO hibernate_sequence VALUES (1);
INSERT INTO hibernate_sequence VALUES (1);
INSERT INTO hibernate_sequence VALUES (1);
INSERT INTO hibernate_sequence VALUES (1);
INSERT INTO hibernate_sequence VALUES (1);
INSERT INTO hibernate_sequence VALUES (1);
CREATE TABLE Language (
  id             BIGINT       NOT NULL,
  enabled        BOOLEAN,
  locale         VARCHAR(255) NOT NULL,
  name           VARCHAR(255) NOT NULL,
  project_id     BIGINT,
  translation_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE Project (
  id          BIGINT       NOT NULL,
  createdDate DATE,
  description VARCHAR(255),
  name        VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE Translation (
  id         BIGINT       NOT NULL,
  version    VARCHAR(255) NOT NULL,
  project_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE User (
  id                    BIGINT               NOT NULL,
  accountNonExpired     BOOLEAN DEFAULT TRUE NOT NULL,
  accountNonLocked      BOOLEAN DEFAULT TRUE NOT NULL,
  createdDate           DATE,
  credentialsNonExpired BOOLEAN DEFAULT TRUE NOT NULL,
  email                 VARCHAR(255),
  enabled               BOOLEAN DEFAULT TRUE NOT NULL,
  firstname             VARCHAR(255),
  lastname              VARCHAR(255),
  password              VARCHAR(255),
  username              VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE User_authorities (
  User_id        BIGINT NOT NULL,
  authorities_id BIGINT NOT NULL,
  PRIMARY KEY (User_id, authorities_id)
);
CREATE TABLE User_projects (
  User_id     BIGINT NOT NULL,
  projects_id BIGINT NOT NULL,
  PRIMARY KEY (User_id, projects_id)
);
ALTER TABLE Project
  ADD CONSTRAINT UK_iflk2yk9ma95q0q9ovhftpi63 UNIQUE (name);
ALTER TABLE User
  ADD CONSTRAINT UK_jreodf78a7pl5qidfh43axdfb UNIQUE (username);
ALTER TABLE User_authorities
  ADD CONSTRAINT UK_q2w4n906e4yb8iidtwk50q543 UNIQUE (authorities_id);
ALTER TABLE User_projects
  ADD CONSTRAINT UK_m8pn8mdx721dyh0trvdo2cscj UNIQUE (projects_id);
ALTER TABLE Glossary
  ADD CONSTRAINT FKgh2y0dg4okp6nryv54jajwmm7 FOREIGN KEY (language_id) REFERENCES Language (id);
ALTER TABLE Glossary
  ADD CONSTRAINT FKltpporbhaimt0rkanrklb4guf FOREIGN KEY (project_id) REFERENCES Project (id);
ALTER TABLE Language
  ADD CONSTRAINT FKnxyl3gh68k3grky4ac5tfaeoj FOREIGN KEY (project_id) REFERENCES Project (id);
ALTER TABLE Language
  ADD CONSTRAINT FK952w88y87mbn9bigik0k3kejn FOREIGN KEY (translation_id) REFERENCES Translation (id);
ALTER TABLE Translation
  ADD CONSTRAINT FKb178kuo1gxwssgj9q24v40kyg FOREIGN KEY (project_id) REFERENCES Project (id);
ALTER TABLE User_authorities
  ADD CONSTRAINT FKlnjxbwwwophmboqcek0d4s4yv FOREIGN KEY (authorities_id) REFERENCES Authority (id);
ALTER TABLE User_authorities
  ADD CONSTRAINT FKbhqwcab2sxqgj6i3jquq4hnlo FOREIGN KEY (User_id) REFERENCES User (id);
ALTER TABLE User_projects
  ADD CONSTRAINT FK6bgn13eg2kyogqq9rcf0t9e25 FOREIGN KEY (projects_id) REFERENCES Project (id);
ALTER TABLE User_projects
  ADD CONSTRAINT FKdnrbe85qsuou1w6u6qrqahat1 FOREIGN KEY (User_id) REFERENCES User (id);
