package com.github.spanslator;

import com.github.spanslator.entity.Glossary;
import com.github.spanslator.entity.Language;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import com.github.spanslator.service.GlossaryService;
import com.github.spanslator.service.LanguageService;
import com.github.spanslator.service.ProjectService;
import com.github.spanslator.service.TranslationService;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * @author Deepak Sunanda Prabhakar
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class DataLoaders {

	@Resource
	public ProjectService projectService;

	@Resource
	public TranslationService translationService;

	@Resource
	public LanguageService languageService;

	@Resource
	public GlossaryService glossaryService;

	@Test
	public void nameSakeTest() {
		boolean check = true;
		assertTrue(check);
	}

	public void loadAllData() {
		loadDummyGlossaryToLanguages();
		boolean check = true;
		assertTrue(check);
	}

	public Project loadTestProject() {
		Project project = new Project();
		project.setName("Test Project");
		project.setDescription("This is a dummy description");
		projectService.saveAndFlush(project);
		return project;
	}

	public boolean cleanUpProject(Long projectId) {
		projectService.delete(projectId);
		return !projectService.findById(projectId).isPresent();
	}

	public List<Translation> loadTranslationsForTestProject() {
		Project project = loadTestProject();
		Translation t1 = new Translation("1.0");
		Translation t2 = new Translation("1.1");
		t1.setProject(project);
		t2.setProject(project);
		List<Translation> translations = new ArrayList();
		translations.add(t1);
		translations.add(t2);
		translationService.saveAll(translations);
		return translations;
	}

	public boolean cleanUpTranslationsForTestProject(Long projectId) {
		List<Translation> translations = translationService.findAllByProjectId(projectId);
		translationService.deleteAll(translations);
		return translationService.findAllByProjectId(projectId).isEmpty() && cleanUpProject(projectId);
	}

	public List<Language> loadLanguagesToTranslationsOfTestProject() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		Optional<Translation> translation = translationService
				.findByVersion("1.0", project.getId());
		Language l1 = new Language("English", "en_US");
		l1.setProject(project);
		l1.setEnabled(true);
		l1.setTranslation(translation.get());
		Language l2 = new Language("English", "en_GB");
		l2.setProject(project);
		l2.setEnabled(false);
		l2.setTranslation(translation.get());
		languageService.saveAndFlush(l1);
		languageService.saveAndFlush(l2);
		List<Language> languages = new ArrayList();
		languages.add(l1);
		languages.add(l2);
		return languages;
	}

	public boolean cleanUpLanguages(Long projectId) {
		List<Language> languages = languageService.getLanguagesByProjectId(projectId);
		languageService.deleteAll(languages);
		languages = languageService.getLanguagesByProjectId(projectId);
		return languages.isEmpty() && cleanUpTranslationsForTestProject(projectId);
	}

	public void loadGlossaryToLanguages() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		Language lang_en_us = languageService.getEnabledLanguageByLocaleAndProjectId("en_US", project.getId());
		Optional<Translation> translations = translationService.findByVersion("1.0", project.getId());
		Glossary g1 = new Glossary("source.text.one", "Translated Text One");
		g1.setProject(project);
		g1.setLanguage(lang_en_us);
		g1.setTranslation(translations.get());
		Glossary g2 = new Glossary("source.text.two", "Translated Text Two");
		g2.setProject(project);
		g2.setLanguage(lang_en_us);
		g2.setTranslation(translations.get());
		glossaryService.saveAndFlush(g1);
		glossaryService.saveAndFlush(g2);
	}

	public void loadDummyGlossaryToLanguages() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		Language lang_en_us = languageService.getEnabledLanguageByLocaleAndProjectId("en_US", project.getId());
		Optional<Translation> translations = translationService.findByVersion("1.0", project.getId());
		List<Glossary> glossaries = new ArrayList();
		for (int i = 0; i <= 1000; i++) {
			Glossary g1 = new Glossary(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(10));
			g1.setProject(project);
			g1.setLanguage(lang_en_us);
			g1.setTranslation(translations.get());
			glossaryService.saveAndFlush(g1);
			glossaries.add(g1);
		}
	}

	public boolean cleanUpGlossaries(Long projectId, String locale) {
		List<Glossary> languages = glossaryService.getGlossariesForProjectIdAndLanguage(projectId, locale);
		glossaryService.deleteAll(languages);
		return glossaryService.getGlossariesForProjectIdAndLanguage(projectId, "en_US").isEmpty() && cleanUpLanguages(
				projectId);
	}

}
