package com.github.spanslator.service;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Language;
import com.github.spanslator.entity.Project;
import org.junit.Test;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Deepak Sunanda Prabhakar
 */
public class LanguageServiceTest extends DataLoaders {

	@Test
	@Transactional
	public void testGetListOfAllLanguagesInProject() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		List<Language> languages = languageService
				.getLanguagesByProjectIdAndTranslationVersion(project.getId(), "1.0");
		assertThat(languages.size()).isEqualTo(2);
		languages = languageService.getLanguagesByProjectIdAndTranslationVersion(project.getId(), "1.1");
		assertThat(languages.size()).isEqualTo(0);
	}

	@Test
	@Transactional
	public void testFindAllByEnabledAndProjectId() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		List<Language> languages = languageService.getEnabledByProjectIdAndTranslationVersion(project.getId(), "1.0");
		assertThat(languages.size()).isEqualTo(1);
	}

	@Test
	@Transactional
	public void testFindAllByNotEnabledAndProjectId() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		List<Language> languages = languageService.getDisabledByProjectIdAndTranslationVersion(project.getId(), "1.0");
		assertThat(languages.size()).isEqualTo(1);
	}
}