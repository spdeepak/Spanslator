package com.github.spanslator.service;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Project;
import org.junit.Test;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author Deepak Sunanda Prabhakar
 */
public class ProjectServiceTest extends DataLoaders {

	@Test
	@Transactional
	public void testFindById() {
		Optional<Project> pr = projectService.findById(loadTestProject().getId());
		assertTrue(pr.isPresent());
		Project project = pr.get();
		assertThat(project.getName()).isEqualTo("Test Project");
	}

	@Test
	@Transactional
	public void testFindByName() {
		loadTestProject();
		Project pr = projectService.findByName("Test Project");
		assertThat(pr.getName()).isEqualTo("Test Project");
	}
}
