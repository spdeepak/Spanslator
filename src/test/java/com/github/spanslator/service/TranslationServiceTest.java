package com.github.spanslator.service;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import org.junit.Test;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Deepak Sunanda Prabhakar
 */
public class TranslationServiceTest extends DataLoaders {

	@Test
	@Transactional
	public void testFindByVersion() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		Optional<Translation> translations = translationService
				.findByVersion("1.0", project.getId());
		assertThat(translations.isPresent()).isTrue();
		translations = translationService.findByVersion("1.1", project.getId());
		assertThat(translations.isPresent()).isTrue();
	}

	@Test
	@Transactional
	public void testFindByProjectId() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		List<Translation> translations = translationService.findAllByProjectId(project.getId());
		assertThat(translations.size()).isEqualTo(2);
		List<Translation> translation = translations
				.stream()
				.filter(t -> t.getVersion().equals("1.0"))
				.collect(Collectors.toList());
		assertThat(translation.size()).isEqualTo(1);
		assertThat(translation.get(0).getProject().getId()).isEqualTo(project.getId());
	}
}