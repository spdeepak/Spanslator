package com.github.spanslator.service;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Glossary;
import com.github.spanslator.entity.Project;
import org.junit.Test;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Deepak Sunanda Prabhakar
 */
public class GlossariesServiceTest extends DataLoaders {

	@Test
	@Transactional
	public void testGetAllGlossariesForProjectIdAndLanguageLocale() {
		loadGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		List<Glossary> glossaries = glossaryService
				.getGlossariesForProjectIdAndLanguage(project.getId(), "en_US");
		assertThat(glossaries.size()).isEqualTo(2);
	}

	@Test
	@Transactional
	public void testFindByProjectIdLocaleSourceText() {
		loadGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		Glossary glossary = glossaryService
				.findByProjectIdLocaleSourceText(project.getId(), "en_US", "source.text.one");
		assertThat(glossary.getId()).isNotNull();
		assertThat(glossary.getLanguage().getName()).isEqualTo("English");
		assertThat(glossary.getProject().getId()).isEqualTo(project.getId());
		assertThat(glossary.getLanguage().getLocale()).isEqualTo("en_US");
		assertThat(glossary.getSourceText()).isEqualTo("source.text.one");
		assertThat(glossary.getTranslatedText()).isEqualTo("Translated Text One");
	}

	@Test
	@Transactional
	public void testFindByProjectIdLocaleVersion() {
		loadGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		List<Glossary> glossaries = glossaryService.findByProjectIdLocaleVersion(project.getId(), "en_US", "1.0");
		assertThat(glossaries.size()).isEqualTo(2);
		for (Glossary glossary : glossaries) {
			assertThat(glossary.getId()).isNotNull();
			assertThat(glossary.getSourceText()).isIn("source.text.one", "source.text.two");
			assertThat(glossary.getTranslatedText()).isIn("Translated Text One", "Translated Text Two");
			assertThat(glossary.getLanguage().getLocale()).isEqualTo("en_US");
			assertThat(glossary.getLanguage().getName()).isEqualTo("English");
			assertThat(glossary.getProject().getId()).isEqualTo(project.getId());
		}
	}

	@Test
	@Transactional
	public void testFindByProjectIdLocaleVersionFor1000GLossary() {
		loadDummyGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		long startTime = System.currentTimeMillis();
		List<Glossary> glossaries = glossaryService.findByProjectIdLocaleVersion(project.getId(), "en_US", "1.0");
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		assertThat(elapsedTime).isLessThan(100L);
		assertThat(glossaries.size()).isEqualTo(1001);
	}

	@Test
	@Transactional
	public void testFindByProjectIdLocaleVersionSourceText() {
		loadGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		Glossary glossary = glossaryService
				.findByProjectIdLocaleVersionSourceText(project.getId(), "en_US", "1.0", "source.text.one");
		assertThat(glossary.getId()).isNotNull();
		assertThat(glossary.getLanguage().getName()).isEqualTo("English");
		assertThat(glossary.getProject().getId()).isEqualTo(project.getId());
		assertThat(glossary.getLanguage().getLocale()).isEqualTo("en_US");
		assertThat(glossary.getSourceText()).isEqualTo("source.text.one");
		assertThat(glossary.getTranslatedText()).isEqualTo("Translated Text One");
	}
}