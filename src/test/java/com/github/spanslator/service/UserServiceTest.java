package com.github.spanslator.service;

import com.github.spanslator.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * @author Deepak Sunanda Prabhakar
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class UserServiceTest {

	@Resource
	private UserService userService;

	@Test
	public void testLoadByUserName() {
		User user = (User) userService.loadUserByUsername("spdeepak");
		assertThat(user.getEmail()).isEqualTo("speedpak1991@gmail.com");
		assertThat(user.getFirstname()).isEqualTo("deepak");
		assertThat(user.getLastname()).isEqualTo("sp");
	}
}