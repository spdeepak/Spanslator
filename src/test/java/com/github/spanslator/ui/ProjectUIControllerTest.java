package com.github.spanslator.ui;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Project;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectUIControllerTest extends DataLoaders {

	@Autowired
	ProjectUIController projectUIController;

	@Test
	public void testCreateProjectForm() {
		Model modelMap = new ExtendedModelMap();
		String url = projectUIController.createProjectForm(modelMap);
		assertThat(url).isEqualTo("createProject");
		assertThat(modelMap.containsAttribute("project")).isTrue();
		Map<String, Object> map = modelMap.asMap();
		assertThat(map.size()).isEqualTo(1);
		assertThat(map.get("project").getClass()).isEqualTo(Project.class);
	}

	@Test
	public void testCreateProjectSubmit() {
		Project project = new Project("Test Project");
		Model modelMap = new ExtendedModelMap();
		Map<Object, Object> map = new HashMap();
		BindingResult bindingResult = new MapBindingResult(map, "project");
		String url = projectUIController.createProjectSubmit(project, modelMap, bindingResult);
		assertThat(url).isEqualTo("redirect:/");
		assertThat(project.getId()).isNotNull();
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateProjectSubmitFail() {
		Project existingProject = loadTestProject();
		Project project = new Project("Test Project");
		Model modelMap = new ExtendedModelMap();
		Map<Object, Object> map = new HashMap();
		BindingResult bindingResult = new MapBindingResult(map, "project");
		String url = projectUIController.createProjectSubmit(project, modelMap, bindingResult);
		assertThat(url).isEqualTo("createProject");
		assertThat(bindingResult.getFieldErrorCount()).isEqualTo(1);
		FieldError fieldError = bindingResult.getFieldError();
		assertThat(fieldError.getObjectName()).isEqualTo("project");
		assertThat(fieldError.getField()).isEqualTo("name");
		assertThat(fieldError.getDefaultMessage())
				.isEqualTo("Project with name ".concat(existingProject.getName().concat(" already exists")));
		assertThat(cleanUpProject(existingProject.getId())).isTrue();
	}
}