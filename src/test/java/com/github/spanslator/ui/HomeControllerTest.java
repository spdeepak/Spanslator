package com.github.spanslator.ui;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Project;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class HomeControllerTest extends DataLoaders {

	@Autowired
	private HomeController homeController;

	@Test
	public void testGotoHomePage() {
		loadTestProject();
		Project project = projectService.findByName("Test Project");
		Model modelMap = new ExtendedModelMap();
		String url = homeController.gotoHomePage(modelMap);
		assertThat(url).isEqualTo("home");
		assertThat(modelMap.containsAttribute("projects")).isTrue();
		assertThat(modelMap.containsAttribute("project")).isTrue();
		Map<String, Object> map = modelMap.asMap();
		assertThat(map.size()).isEqualTo(2);
		assertThat(map.get("project").getClass()).isEqualTo(Project.class);
		assertThat(map.get("projects").getClass()).isEqualTo(ArrayList.class);
		assertThat(((List<Project>) map.get("projects")).size()).isEqualTo(1);
		assertThat(cleanUpProject(project.getId())).isTrue();
	}
}