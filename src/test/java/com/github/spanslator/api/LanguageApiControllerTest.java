package com.github.spanslator.api;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Language;
import com.github.spanslator.entity.Project;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Deepak Sunanda Prabhakar
 */
@TestPropertySource(locations = "classpath:test-application.properties")
public class LanguageApiControllerTest extends DataLoaders {

	@Resource
	private TestRestTemplate testRestTemplate;

	@Value("${admin.username}")
	private String username;

	@Value("${admin.password}")
	private String password;

	@Test
	public void testGetAllLanguagesForProject() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/translation/1.0/languages";
		ResponseEntity<Language[]> languagesEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Language[].class);
		assertThat(languagesEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		Language[] languages = languagesEntity.getBody();
		for (Language language : languages) {
			assertThat(language.getName()).isEqualTo("English");
			assertThat(language.getLocale()).isIn("en_US", "en_GB");
			assertThat(language.isEnabled()).isIn(true, false);
		}
		assertThat(cleanUpLanguages(project.getId())).isTrue();
	}

	@Test
	public void testGetAllLanguagesForProjectFail() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/translation/1.1/languages";
		ResponseEntity<ApiResponse> languagesEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(languagesEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

		ApiResponse response = languagesEntity.getBody();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getMessage()).isEqualTo("No JustLanguages were found for this project");

		List<Language> languages = languageService
				.getLanguagesByProjectIdAndTranslationVersion(project.getId(), "1.0");
		assertThat(languages.size()).isEqualTo(2);
		assertThat(cleanUpLanguages(project.getId())).isTrue();
	}

	@Test
	public void testGetEnabledLanguagesForProject() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/translation/1.0/language/enabled";
		ResponseEntity<Language[]> languagesEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Language[].class);
		assertThat(languagesEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(languagesEntity.getBody().length).isEqualTo(1);
		Language language = languagesEntity.getBody()[0];
		assertThat(language.getName()).isEqualTo("English");
		assertThat(language.getLocale()).isEqualTo("en_US");
		languageService.delete(language.getId());
		List<Language> languages = languageService.getDisabledByProjectIdAndTranslationVersion(project.getId(), "1.0");
		assertThat(languages.size()).isEqualTo(1);
		assertThat(cleanUpLanguages(project.getId())).isTrue();
	}

	@Test
	public void testGetEnabledLanguagesForProjectFail() {
		String url = "/api/project/1/translation/ver/language/enabled";
		ResponseEntity<ApiResponse> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse response = responseEntity.getBody();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getMessage())
				.isEqualTo("No JustLanguages are enabled for the project(1) and JustTranslations Version(ver)");
	}

	@Test
	public void testGetDisabledLanguagesForProject() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/translation/1.0/language/disabled";
		ResponseEntity<Language[]> languagesEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Language[].class);
		assertThat(languagesEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(languagesEntity.getBody().length).isEqualTo(1);
		Language language = languagesEntity.getBody()[0];
		assertThat(language.isEnabled()).isFalse();
		assertThat(language.getName()).isEqualTo("English");
		assertThat(language.getLocale()).isEqualTo("en_GB");
		languageService.delete(language.getId());
		List<Language> languages = languageService.getEnabledByProjectIdAndTranslationVersion(project.getId(), "1.0");
		assertThat(languages.size()).isEqualTo(1);
		assertThat(cleanUpLanguages(project.getId())).isTrue();
	}

	@Test
	public void testGetDisabledLanguagesForProjectFail() {
		String url = "/api/project/1/translation/ver/language/disabled";
		ResponseEntity<ApiResponse> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse response = responseEntity.getBody();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getMessage())
				.isEqualTo("No Languages are disabled for the project(1) and JustTranslations Version(ver)");
	}

	@Test
	public void testCreateLanguage() {
		loadTranslationsForTestProject();
		Map<String, Object> request = new HashMap();
		request.put("name", "Kannada");
		request.put("locale", "kn");
		request.put("enabled", true);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/translation/1.0/language";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		String body = "Language with name(" + request.get("name") + ") and locale(" + request.get("locale")
				+ ") is created";
		assertThat(response.getBody().get("status")).isEqualTo(HttpStatus.CREATED.getReasonPhrase().toUpperCase());
		assertThat(response.getBody().get("message")).isEqualTo(body);
		assertThat(cleanUpLanguages(project.getId())).isTrue();
	}

	@Test
	public void testCreateLanguageWithNoProject() {
		Map<String, Object> request = new HashMap();
		request.put("name", "Kannada");
		request.put("locale", "kn");
		request.put("enabled", true);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);
		String url = "/api/project/1/translation/1.0/language";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo(HttpStatus.NOT_FOUND.name());
		assertThat(response.getBody().get("message"))
				.isEqualTo("No Project found with ID: 1");
	}

	@Test
	public void testCreateLanguageWithNoTranslationsVersion() {
		Project project = loadTestProject();
		Map<String, Object> request = new HashMap();
		request.put("name", "Kannada");
		request.put("locale", "kn");
		request.put("enabled", true);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);
		String url = "/api/project/" + project.getId() + "/translation/1.0/language";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo(HttpStatus.NOT_FOUND.name());
		assertThat(response.getBody().get("message"))
				.isEqualTo("No JustTranslations with version (1.0) found in Project with ID: "
						+ project.getId());
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateLanguageWithNullName() {
		loadTranslationsForTestProject();
		Map<String, Object> request = new HashMap();
		request.put("name", null);
		request.put("locale", "kn");
		request.put("enabled", true);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		Project project = projectService.findByName("Test Project");

		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);
		String url = "/api/project/" + project.getId() + "/translation/1.0/language";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);
		assertThat(response.getBody().get("status")).isEqualTo(HttpStatus.NOT_ACCEPTABLE.name());
		assertThat(response.getBody().get("message"))
				.isEqualTo("Language name is mandatory");
		assertThat(cleanUpTranslationsForTestProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateLanguageWithNullLocale() {
		loadTranslationsForTestProject();
		Map<String, Object> request = new HashMap();
		request.put("name", "Kannada");
		request.put("locale", null);
		request.put("enabled", true);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		Project project = projectService.findByName("Test Project");

		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);
		String url = "/api/project/" + project.getId() + "/translation/1.0/language";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);
		assertThat(response.getBody().get("status")).isEqualTo(HttpStatus.NOT_ACCEPTABLE.name());
		assertThat(response.getBody().get("message"))
				.isEqualTo("Language Locale is mandatory");
		assertThat(cleanUpTranslationsForTestProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateLanguageWithNullNameAndLocale() {
		loadTranslationsForTestProject();
		Map<String, Object> request = new HashMap();
		request.put("name", null);
		request.put("locale", null);
		request.put("enabled", true);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		Project project = projectService.findByName("Test Project");

		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);
		String url = "/api/project/" + project.getId() + "/translation/1.0/language";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);
		assertThat(response.getBody().get("status")).isEqualTo(HttpStatus.NOT_ACCEPTABLE.name());
		assertThat(response.getBody().get("message"))
				.isEqualTo("Language Name and Locale is Mandatory");
		assertThat(cleanUpTranslationsForTestProject(project.getId())).isTrue();
	}
}