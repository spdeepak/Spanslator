package com.github.spanslator.api;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Glossary;
import com.github.spanslator.entity.Project;
import com.github.spanslator.util.FormString;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.LinkedMultiValueMap;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Deepak Sunanda Prabhakar
 */
@TestPropertySource(locations = "classpath:test-application.properties")
public class GlossariesApiControllerTest extends DataLoaders {

	@Resource
	private TestRestTemplate testRestTemplate;

	@Value("${admin.username}")
	private String username;

	@Value("${admin.password}")
	private String password;

	@Test
	public void testUpdateGlossary() {
		loadGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		Map<String, String> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "New Translated Text One");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";

		Glossary glossary = glossaryService
				.findByProjectIdLocaleVersionSourceText(project.getId(), "en_US", "1.0", request.get("sourceText"));
		assertThat(glossary.getTranslatedText()).isNotEqualTo(request.get("translatedText"));
		assertThat(glossary.getTranslatedText()).isEqualTo("Translated Text One");

		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getMessage())
				.isEqualTo(
						"Glossary Updated for Language en_US in Translation version 1.0's , Project(" + project.getId()
								+ ")");
		glossary = glossaryService
				.findByProjectIdLocaleVersionSourceText(project.getId(), "en_US", "1.0", request.get("sourceText"));
		assertThat(glossary.getTranslatedText()).isEqualTo(request.get("translatedText"));
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();

	}

	@Test
	public void testUpdateGlossaryGlossaryNotFound() {
		loadGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		Map<String, String> request = new HashMap();
		request.put("sourceText", "source.text");
		request.put("translatedText", "New Translated Text One");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";

		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getMessage())
				.isEqualTo(
						"Glossary Source Text(" + request.get("sourceText")
								+ ") not found for Language en_US in Translation version 1.0's , Project(" + project
								.getId() + ")");
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();
	}

	@Test
	public void testUpdateGlossaryProjectNotFound() {
		Map<String, Object> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "Translated Text One");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/1/translation/1.0/language/en_US/glossary";

		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getMessage()).isEqualTo("Project with id 1 Not found");
	}

	@Test
	public void testUpdateGlossaryTranslationsVersionNotFound() {
		Project project = loadTestProject();
		Map<String, Object> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "Translated Text One");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";

		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getMessage()).isEqualTo("Translation version 1.0 Not found");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testUpdateGlossaryLanguageLocaleNotFound() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		Map<String, Object> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "Translated Text One");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";

		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getMessage()).isEqualTo("Language locale en_US Not found");
		assertThat(cleanUpTranslationsForTestProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateGlossary() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		Map<String, Object> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "Translated Text One");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(response.getBody().get("status")).isEqualTo(HttpStatus.CREATED.getReasonPhrase().toUpperCase());
		assertThat(response.getBody().get("message"))
				.isEqualTo(
						"Glossary created for Language en_US in Translation version 1.0's , Project(" + project.getId()
								+ ")");
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();
	}

	@Test
	public void testCreateGlossaryProjectIdFail() {
		Map<String, Object> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "Translated Text One");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/1/translation/1.0/language/en_US/glossary";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo("NOT_FOUND");
		assertThat(response.getBody().get("message"))
				.isEqualTo("Project with id 1 Not found");
	}

	@Test
	public void testCreateGlossaryTranslationsVersionFail() {
		Project project = loadTestProject();
		Map<String, Object> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "Translated Text One");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo("NOT_FOUND");
		assertThat(response.getBody().get("message"))
				.isEqualTo("Translation version 1.0 Not found");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateGlossaryLanguageLocaleFail() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		Map<String, Object> request = new HashMap();
		request.put("sourceText", "source.text.one");
		request.put("translatedText", "Translated Text One");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";

		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo("NOT_FOUND");
		assertThat(response.getBody().get("message"))
				.isEqualTo("Language locale en_US Not found");
		assertThat(cleanUpTranslationsForTestProject(project.getId())).isTrue();
	}

	@Test
	public void testGetAllGlossariesForProjectAndLocale() {
		loadGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/language/en_US/glossary";
		ResponseEntity<Glossary[]> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Glossary[].class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		Glossary[] glossaries = responseEntity.getBody();
		for (Glossary glossary : glossaries) {
			assertThat(glossary.getId()).isNotNull();
			assertThat(glossary.getSourceText()).isIn("source.text.one", "source.text.two");
			assertThat(glossary.getTranslatedText()).isIn("Translated Text One", "Translated Text Two");
			glossaryService.delete(glossary.getId());
		}
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();
	}

	@Test
	public void testGetAllGlossariesForProjectAndLocaleFail() {
		String url = "/api/project/1/language/en_US/glossary";
		ResponseEntity<ApiResponse> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse response = responseEntity.getBody();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getMessage())
				.isEqualTo("Not Glossaries of locale(en_US) were not found for Project with id 1");
	}

	@Test
	public void testGetAllGlossariesByProjectIdLanguageLocaleTranslationVersion() {
		loadDummyGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary";
		long start = System.currentTimeMillis();
		ResponseEntity<Glossary[]> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Glossary[].class);
		long end = System.currentTimeMillis();
		System.out.println(end - start);
		assertThat(end - start).isLessThan(300L);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		Glossary[] glossaries = responseEntity.getBody();
		assertThat(glossaries.length).isEqualTo(1001);
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();
	}

	@Test
	public void testGetAllGlossariesByProjectIdLanguageLocaleTranslationVersionFail() {
		String url = "/api/project/1/translation/1.0/language/en_US/glossary";
		ResponseEntity<ApiResponse> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse response = responseEntity.getBody();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getMessage()).isEqualTo(
				"Not Glossaries of locale(en_US) were not found for Project with id 1 And Translation Version 1.0");
	}

	@Test
	public void testGetAllGlossariesAsMapByProjectIdLanguageLocaleTranslationVersion() {
		loadDummyGlossaryToLanguages();
		Project project = projectService.findByName("Test Project");
		String url = "/api/project/" + project.getId() + "/translation/1.0/language/en_US/glossary/map";
		long start = System.currentTimeMillis();
		ResponseEntity<Map> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Map.class);
		long end = System.currentTimeMillis();
		System.out.println(end - start);
		assertThat(end - start).isLessThan(500L);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		Map<String, String> map = responseEntity.getBody();
		assertThat(map.isEmpty()).isFalse();
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();
	}

	@Test
	public void testGetAllGlossariesAsMapByProjectIdLanguageLocaleTranslationVersionFail() {
		String url = "/api/project/1/translation/1.0/language/en_US/glossary/map";
		ResponseEntity<ApiResponse> responseEntity = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse response = responseEntity.getBody();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getMessage()).isEqualTo(
				"Not Glossaries of locale(en_US) were not found for Project with id 1 And Translation Version 1.0");
	}

	@Test
	public void testCreateGlossariesFromFile() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		String url = FormString.from("/api/project/", String.valueOf(project.getId()),
				"/translation/1.0/language/en_US/glossary/file");
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap();
		parameters.add("file", new ClassPathResource("glossary.txt"));
		parameters.add("lineSeparator", "=");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(parameters, headers);
		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(response.getBody().get("status")).isEqualTo("CREATED");
		assertThat(response.getBody().get("message"))
				.isEqualTo(
						"Successfully created 2 Glossaries for Locale en_US, Translation version 1.0 in Project Test Project");
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();
	}

	@Test
	public void testCreateGlossariesFromFileWhileFileNull() {
		String url = FormString.from("/api/project/1/translation/1.0/language/en_US/glossary/file");
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap();
		parameters.add("file", new ClassPathResource("glossary.txt"));
		parameters.add("lineSeparator", "=");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(parameters, headers);
		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo("NOT_FOUND");
		assertThat(response.getBody().get("message")).isEqualTo("Project with id 1 not found");
	}

	@Test
	public void testCreateGlossariesFromFileWhileTranslationNotPresent() {
		Project project = loadTestProject();
		String url = FormString.from("/api/project/" + String.valueOf(project.getId())
				+ "/translation/1.0/language/en_US/glossary/file");
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap();
		parameters.add("file", new ClassPathResource("glossary.txt"));
		parameters.add("lineSeparator", "=");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(parameters, headers);
		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo("NOT_FOUND");
		assertThat(response.getBody().get("message")).isEqualTo("Translation with version 1.0 not found");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateGlossariesFromFileWhileLocaleNotPresent() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		String url = FormString.from("/api/project/" + String.valueOf(project.getId())
				+ "/translation/1.0/language/en_US/glossary/file");
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap();
		parameters.add("file", new ClassPathResource("glossary.txt"));
		parameters.add("lineSeparator", "=");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(parameters, headers);
		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().get("status")).isEqualTo("NOT_FOUND");
		assertThat(response.getBody().get("message")).isEqualTo("Language with Locale en_US not found");
		assertThat(cleanUpTranslationsForTestProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateGlossariesFromFileFailWithNoGlossaries() {
		loadLanguagesToTranslationsOfTestProject();
		Project project = projectService.findByName("Test Project");
		String url = FormString.from("/api/project/", String.valueOf(project.getId()),
				"/translation/1.0/language/en_US/glossary/file");
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap();
		parameters.add("file", new ClassPathResource("empty.txt"));
		parameters.add("lineSeparator", "=");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(parameters, headers);
		ResponseEntity<Map> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, Map.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		//assertThat(response.getBody().get("status")).isEqualTo("NO_CONTENT");
		//assertThat(response.getBody().get("message")).isEqualTo("Given file does not contain valid Glossaries");
		assertThat(cleanUpGlossaries(project.getId(), "en_US")).isTrue();
	}

}