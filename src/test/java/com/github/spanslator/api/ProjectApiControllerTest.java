package com.github.spanslator.api;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Project;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Deepak Sunanda Prabhakar
 */
@TestPropertySource(locations = "classpath:test-application.properties")
public class ProjectApiControllerTest extends DataLoaders {

	@Resource
	private TestRestTemplate testRestTemplate;

	@Value("${admin.username}")
	private String username;

	@Value("${admin.password}")
	private String password;

	@Test
	public void testGetProjectById() {
		Project project = loadTestProject();
		Long id = project.getId();
		String url = "/api/project/".concat(String.valueOf(id));

		ResponseEntity<Project> pr = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Project.class);

		assertThat(pr.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(pr.getBody().getId()).isNotNull();
		assertThat(pr.getBody().getName()).isEqualTo("Test Project");
		assertThat(pr.getBody().getCreatedDate()).isNotNull();
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testGetProjectByIdFail() {
		Long id = Long.MAX_VALUE;
		String url = "/api/project/" + id;
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody().getMessage()).isEqualTo("Project with id(" + id + ") was not found");
	}

	@Test
	public void testGetProjects() {
		Project project = loadTestProject();
		String url = "/api/projects";

		ResponseEntity<Project[]> pr = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Project[].class);

		assertThat(pr.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(pr.getBody()[0].getId()).isNotNull();
		assertThat(pr.getBody()[0].getName()).isEqualTo("Test Project");
		assertThat(pr.getBody()[0].getCreatedDate()).isNotNull();

		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testGetProjectsFail() {
		String url = "/api/projects";

		ResponseEntity<ApiResponse> pr = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);

		assertThat(pr.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(pr.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(pr.getBody().getMessage()).isEqualTo("No projects were found");
	}

	@Test
	public void testCreateProjectPass() {
		Map<String, String> request = new HashMap();
		request.put("name", "POST Test Project");
		request.put("description", "This is a Dummy Description for The POST Test Project");

		ResponseEntity<Map> responseEntity = postToCreateProject(request);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);

		Map<String, String> response = responseEntity.getBody();
		Project project = projectService.findByName(request.get("name"));

		assertThat(response.get("status")).isEqualTo("CREATED");
		assertThat(response.get("message"))
				.isEqualTo(request.get("name").concat(" is created with id " + project.getId()));

		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateExistingProjectFail() {
		loadTestProject();
		Map<String, String> request = new HashMap();
		request.put("name", "Test Project");
		request.put("description", null);

		ResponseEntity<Map> responseEntity = postToCreateProject(request);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);

		Map<String, String> response = responseEntity.getBody();
		Project project = projectService.findByName(request.get("name"));

		assertThat(response.get("status")).isEqualTo("NOT_ACCEPTABLE");
		assertThat(response.get("message"))
				.isEqualTo("Project with name (" + request.get("name").concat(") already exists"));
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateProjectNameNullFail() {
		Project project = loadTestProject();
		Map<String, String> request = new HashMap();
		request.put("name", null);
		request.put("description", null);

		ResponseEntity<Map> responseEntity = postToCreateProject(request);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);

		Map<String, String> response = responseEntity.getBody();
		assertThat(response.get("status")).isEqualTo("NOT_ACCEPTABLE");
		assertThat(response.get("message"))
				.isEqualTo("Project name is Mandatory");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	private ResponseEntity<Map> postToCreateProject(Map<String, String> request) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(request, headers);

		return testRestTemplate.withBasicAuth(username, password)
				.postForEntity("/api/project", entity, Map.class);
	}

	@Test
	public void testUpdateProject() {
		Project project = loadTestProject();
		Map<String, String> request = new HashMap();
		request.put("name", "New Test Project Name");
		request.put("description", "New Test Project Description");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + project.getId();

		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.OK);
		assertThat(apiResponse.getMessage())
				.isEqualTo("Project Updated, CreatedDate retained the same as during Project Creation");

		Optional<Project> updatedProject = projectService.findById(project.getId());
		assertThat(updatedProject.isPresent()).isTrue();
		assertThat(updatedProject.get().getId()).isEqualTo(project.getId());

		Calendar updated = Calendar.getInstance();
		updated.setTime(updatedProject.get().getCreatedDate());

		Calendar existing = Calendar.getInstance();
		existing.setTime(project.getCreatedDate());

		assertThat(updated.get(Calendar.YEAR)).isEqualTo(existing.get(Calendar.YEAR));
		assertThat(updated.get(Calendar.MONTH)).isEqualTo(existing.get(Calendar.MONTH));
		assertThat(updated.get(Calendar.DATE)).isEqualTo(existing.get(Calendar.DATE));

		assertThat(updatedProject.get().getName()).isEqualTo(request.get("name"));
		assertThat(updatedProject.get().getDescription()).isEqualTo(request.get("description"));
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testUpdateProjectNameNull() {
		Project project = loadTestProject();
		Map<String, String> request = new HashMap();
		request.put("name", null);
		request.put("description", "New Test Project Description");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + project.getId();
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.FORBIDDEN);
		assertThat(apiResponse.getMessage()).isEqualTo("Updated project name cannot be Null");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testUpdateProjectIdNull() {
		Project project = loadTestProject();
		Map<String, Object> request = new HashMap();
		request.put("id", 1234566);
		request.put("name", "New Test Project name");
		request.put("description", "New Test Project Description");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + project.getId();
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.FORBIDDEN);
		assertThat(apiResponse.getMessage()).isEqualTo("Cannot change the ID of a Project");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testUpdateProjectIdNotFound() {
		Map<String, Object> request = new HashMap();
		request.put("name", "New Test Project name");
		request.put("description", "New Test Project Description");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + Long.MAX_VALUE;
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(apiResponse.getMessage()).isEqualTo("Project with ID (" + Long.MAX_VALUE + ") not found");
	}

	@Test
	public void testUpdateProjectDescriptionNull() {
		Project project = loadTestProject();
		Map<String, String> request = new HashMap();
		request.put("name", "New Test Project name");
		request.put("description", null);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + project.getId();
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.FORBIDDEN);
		assertThat(apiResponse.getMessage()).isEqualTo("Description can be empty bit not null");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}

	@Test
	public void testUpdateProjectNameEmpty() {
		Project project = loadTestProject();
		Map<String, String> request = new HashMap();
		request.put("name", "");
		request.put("description", "New Test Project Description");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(request, headers);

		String url = "/api/project/" + project.getId();
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.exchange(url, HttpMethod.PUT, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.FORBIDDEN);
		assertThat(apiResponse.getMessage()).isEqualTo("Updated project name cannot be empty");
		assertThat(cleanUpProject(project.getId())).isTrue();
	}
}