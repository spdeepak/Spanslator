package com.github.spanslator.api;

import com.github.spanslator.DataLoaders;
import com.github.spanslator.entity.Project;
import com.github.spanslator.entity.Translation;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Deepak Sunanda Prabhakar
 */
@TestPropertySource(locations = "classpath:test-application.properties")
public class TranslationApiControllerTest extends DataLoaders {

	@Resource
	private TestRestTemplate testRestTemplate;

	@Value("${admin.username}")
	private String username;

	@Value("${admin.password}")
	private String password;

	@Test
	public void testGetAllTranslations() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		Long projectId = project.getId();
		String url = "/api/project/".concat(String.valueOf(projectId)).concat("/translations");
		ResponseEntity<Translation[]> translations = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Translation[].class);
		assertThat(translations.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(translations.getBody().length).isEqualTo(2);
		assertThat(cleanUpTranslationsForTestProject(projectId)).isTrue();
	}

	@Test
	public void testGetAllTranslationsFail() {
		String url = "/api/project/" + Long.MAX_VALUE + "/translations";
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(apiResponse.getMessage())
				.isEqualTo("No translations were found for Project with ID: " + Long.MAX_VALUE);
	}

	@Test
	public void testGetTranslationsByVersion() {
		loadTranslationsForTestProject();
		Project project = projectService.findByName("Test Project");
		Long projectId = project.getId();
		String url = "/api/project/" + project.getId() + "/translation/1.0/v";
		ResponseEntity<Translation> translations = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Translation.class);
		assertThat(translations.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(translations.getBody().getVersion()).isEqualTo("1.0");
		translationService.delete(translations.getBody());

		url = "/api/project/".concat(String.valueOf(projectId)).concat("/translation/1.1/v");
		translations = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, Translation.class);
		assertThat(translations.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(translations.getBody().getVersion()).isEqualTo("1.1");
		assertThat(cleanUpTranslationsForTestProject(projectId)).isTrue();
	}

	@Test
	public void testGetTranslationsByVersionFail() {
		String url = "/api/project/1/translation/1.0/v";
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.getForEntity(url, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(apiResponse.getMessage())
				.isEqualTo("No translations were found for Project with id 1 and Translation version 1.0");
	}

	@Test
	public void testCreateTranslationsForProjectAndDuplicateCreationTest() {
		Project project = loadTestProject();

		Map<String, Object> request = new HashMap();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/".concat(String.valueOf(project.getId())).concat("/translation/1.0/v");
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.CREATED);
		assertThat(apiResponse.getMessage())
				.isEqualTo("Translation version 1.0 is created for project with id " + project.getId());

		response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);
		apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);
		assertThat(apiResponse.getMessage())
				.isEqualTo(project.getName() + " already has a Translation of version 1.0");

		assertThat(cleanUpTranslationsForTestProject(project.getId())).isTrue();
	}

	@Test
	public void testCreateTranslationsForProjectFail() {
		Map<String, Object> request = new HashMap();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, Object>> entity = new HttpEntity(request, headers);

		String url = "/api/project/1/translation/1.0/v";
		ResponseEntity<ApiResponse> response = testRestTemplate.withBasicAuth(username, password)
				.postForEntity(url, entity, ApiResponse.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		ApiResponse apiResponse = response.getBody();
		assertThat(apiResponse.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(apiResponse.getMessage())
				.isEqualTo("Project Not Found with id 1");
	}
}